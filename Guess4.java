class Guess4{

public static void main(String[] args) 
throws java.io.IOException {
	char ch,ignore,answer = 'K';

	do{

		System.out.print("I'm thinking of a letter between A and Z.");
		System.out.print("Can you guess it: ");
		
		ch = (char) System.in.read();

		//descarta qualquer outro caractere do buffer de entrada
		do{
			ignore = (char) System.in.read();


		}while(ignore != '\n');
		if(ch == answer) System.out.println(" ** RIGHT ** "); 
		else{
			System.out.print(" sorry , you're ");
			if(ch < answer) System.out.print(" too low ");
			else System.out.print(" too high ");
			System.out.print("try again !\n ");
		}

	}while (ch != ch);

}
}

