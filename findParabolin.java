import java.util.*;
class GFG {

	// Method
	// Returning true if string is palindrome
	static boolean isPalindrome(String str)
	{

		// Pointers pointing to the beginning
		// and the end of the string
		int i = 0, j = str.length() - 1;

		// While there are characters to compare
		while (i < j) {

			// If there is a mismatch
			if (str.charAt(i) != str.charAt(j))
				return false;

			// Increment first pointer and
			// decrement the other
			i++;
			j--;
		}

		// Given string is a palindrome
		return true;
	}

	// Method 2
	// main driver method
	public static void main(String[] args)
	{
	    
	   Scanner in = new Scanner(System.in);
        System.out.print("Enter your name or type words and use as delimiter ';' : ");
        String str = in.nextLine();
        String[] textoSeparado = str.split(";");
        
        if (str.length() > 0 ){
            System.out.println("Name is: " + str);
        
        for (String s : str.split(";")) {
           
             	// passing bool function till holding true
    		if (isPalindrome(s))
    
    			// It is a pallindrome
    			System.out.print("Yes " + s + "\n");
    		else
    
    			// Not a pallindrome
    			System.out.print("No " + s + "\n");
             
        }

	
	
	
        }else{
            
			System.out.print("please enter a message ");
            
        }
	    in.close();
	}
	
	
}
